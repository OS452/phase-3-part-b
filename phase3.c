/*
 *  phase3.c
 *
 *	@author: Hercy (Yunhao Zhang)
 *  @author: David Carrig
 *
 */

#include <assert.h>

#include <phase1.h>
#include <phase2.h>
#include <phase3.h>
#include <libuser.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* THE UNIVERSAL DEBUG FLAG */
/* THE UNIVERSAL DEBUG FLAG */
/* THE UNIVERSAL DEBUG FLAG */
#define DEBUG_GLOBAL    1       /**** TURN IT OFF! ****/
/* THE UNIVERSAL DEBUG FLAG */
/* THE UNIVERSAL DEBUG FLAG */
/* THE UNIVERSAL DEBUG FLAG */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */





#define ASSERT          0
#define TOP             0



#define DEBUG_A         1
#define DEBUG_A_DETAIL  0
#define DEBUG_B         1
#define DEBUG_B_DETAIL  0

#define PRINT_CRAPS     0
/*
 * Everybody uses the same tag.
 */
#define TAG 0
/*
 * Page table entry
 */

#define UNUSED      0
#define INCORE      1


#define INVALID     -1
#define VALID       1

#define PROCESS_NAME_LENGTH     20
#define SIZE_OF_FAULT           sizeof(Fault)
#define FIRST_SECTOR            8

/* You'll probably want more states */

/* Page Table Entry */
typedef struct PTE
{
    int		state;		/* See above. */
    int		frame;		/* The frame that stores the page. */
    int		block;		/* The disk block that stores the page. */
    /* Add more stuff here */
    
} PTE;

/*
 * Per-process information
 */
typedef struct Process
{
    int		numPages;	/* Size of the page table. */
    PTE		*pageTable;	/* The page table for the process. */
    /* Add more stuff here if necessary. */
    
} Process;

static Process	processes[P1_MAXPROC];


/*
 * Information about page faults.
 */
typedef struct Fault
{
    int		pid;		/* Process with the problem. */
    void	*addr;		/* Address that caused the fault. */
    int		mbox;		/* Where to send reply. */
    /* Add more stuff here if necessary. */
    
} Fault;

static void	*vmRegion = NULL;

P3_VmStats	P3_vmStats;

static int pagerMbox = -1;
static int Pager(void);
static int getPage(Fault fault);


static void dumpVMStats(void);
static void CheckPid(int);
static void CheckMode(void);
static void FaultHandler(int type, void *arg);

void P3_Fork(int pid);
void P3_Switch(int old, int new);
void P3_Quit(int pid);



static int	numPages = 0;

static int pager_num;
static int pager_kill = 0;
static int * pager_pid_array;


static int	frame_num = 0;


typedef struct Frame
{
    int ID;
    int frame;
    int status;
    int ref;
    P1_Semaphore mutex;
} Frame;

static Frame * frame_list;

static P1_Semaphore running;
static P1_Semaphore complete;

static int pid;

static P1_Semaphore mutex;


static void printVMStats(int debug, char * str);
static P1_Semaphore debug_mutex;




/* PART B */
static int disk_blocks;
static int tracks;
typedef struct Cache
{
    int track;
    int first;
    int sectors;
    void* buffer;
    
    int status;
    
    struct Cache * next;
    
} Cache;

static Cache * frame_cache;

/* Clock algorithm stuffs */
static P1_Semaphore clock_mutex;

static int clock_pointer;



/*
 *----------------------------------------------------------------------
 *
 * P3_VmInit --
 *
 *	Initializes the VM system by configuring the MMU and setting
 *	up the page tables.
 *
 * Results:
 *      MMU return status
 *
 * Side effects:
 *      The MMU is initialized.
 *
 *----------------------------------------------------------------------
 */
/*
 Input
 arg1: number of mappings the MMU should hold
 arg2: number of virtual pages to use
 arg3: number of physical page frames to use
 arg4: number of pager daemons
 
 Output:
 arg1: address of the VM region
 arg4: 0 -- success
 -1 -- illegal values are given as parameters
 -2 -- the VM region has already been initialized
 */
int P3_VmInit(int mappings, int pages, int frames, int pagers)
{
    
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_VmInit: Called\n");
    }
    
    int		status;
    int		tmp;
    int     i;
    
    CheckMode();
    
    // Check VM already been initialized
    if(pagerMbox != INVALID)
    {
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > Virtual memory has already been initialized\n");
        }
        return -2;
    }
    
    
    // Check illegal input, pages & mappings
    if(pagers <= 0 || frames < 0 || pages < 0 || mappings < 0)
    {
        
        USLOSS_Console("ERROR: Invalid input(s) detected\n");
        return INVALID;
    }
    if(pagers > P3_MAX_PAGERS)
    {
        USLOSS_Console("ERROR: Invalid input(s) detected\n");
        return INVALID;
    }
    // Number of mappings should equal the number of virtual pages
    if(pages != mappings)
    {
        
        USLOSS_Console("ERROR: Invalid input(s) detected\n");
        return INVALID;
    }
    
    
    // Initializing datas
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("    > Initializing datas... ");
    }
    numPages = pages;
    
    frame_num = frames;
    frame_list = malloc(sizeof(Frame) * frame_num);
    
    
    /* PART B */
    // TODO
    for(i = 0; i < frame_num; i++)
    {
        frame_list[i].ID = i;
        frame_list[i].status = UNUSED;
        frame_list[i].frame = INVALID;
        frame_list[i].ref = UNUSED;
        frame_list[i].mutex = P1_SemCreate(1);
    }
    tracks = 0;
    
    
    
    /*
    if(pagers == 0)
    {
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("\n      ");
        }
        USLOSS_Console("Warning: Pagers is 0 detected, set to 1\n");
        pager_num = 1;
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > Initializing datas continued... ");
        }
    }
    else
    {
        pager_num = pagers;
    }
    */
    pager_num = pagers;
    //USLOSS_Console("\n    - pagers: %d\n", pagers);
    //USLOSS_Console("    - pager_num: %d\n", pagers);
    pager_pid_array = malloc(sizeof(int) * pager_num);
    for(i = 0; i < pager_num; i++)
    {
        pager_pid_array[i] = -1;
    }
    
    mutex = P1_SemCreate(1);
    complete = P1_SemCreate(0);
    running = P1_SemCreate(0);  // Running semaphore
    
    if((DEBUG_A || DEBUG_B) && DEBUG_GLOBAL)
    {
        debug_mutex = P1_SemCreate(1);
    }
    
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("OK\n");
    }
    
    
    
    // Initialized the MMU
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("    > Initializing the MMU... ");
    }
    status = USLOSS_MmuInit(mappings, pages, frame_num);
    if(status != USLOSS_MMU_OK)
    {
        USLOSS_Console("P3_VmInit: couldn't initialize MMU, status %d\n", status);
        USLOSS_Halt(1);
    }
    else if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("OK\n");
    }
    
    
    vmRegion = USLOSS_MmuRegion(&tmp);
    if(ASSERT)
    {
        assert(vmRegion != NULL);
        assert(tmp >= pages);
    }
    
    USLOSS_IntVec[USLOSS_MMU_INT] = FaultHandler;
    
    for(i = 0; i < P1_MAXPROC; i++)
    {
        processes[i].numPages = 0;
        processes[i].pageTable = NULL;
    
    }
    
    // Create the page fault mailbox and fork the pagers here.
    pagerMbox = P2_MboxCreate(P1_MAXPROC, SIZE_OF_FAULT);
    
    
    
    for(i = 0; i < pager_num; i++)
    {

        
        // Fork the pagers
        char pager_name[PROCESS_NAME_LENGTH];
        
        sprintf(pager_name, "Pager %d", i);
        
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > Forking %s ...\n", pager_name);
        }
        
        pager_pid_array[i] = P1_Fork(pager_name, (int (*)(void *))Pager, NULL, USLOSS_MIN_STACK * 4, P3_PAGER_PRIORITY);
        
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > %s forked\n", pager_name);
        }
        
        // Wait
        P1_P(running);
    }
    
    // DEBUG P3_vmStats
    printVMStats(1, "Before memset");
    
    memset((char *)&P3_vmStats, 0, sizeof(P3_VmStats));
    
    // DEBUG P3_vmStats
    printVMStats(1, "After memset");
    
    // TODO
    
    /* PART B */
    int init_sector, init_track, init_disk;
    int xcode_DS = P2_DiskSize(1, &init_sector, &init_track, &init_disk);
    
    if(xcode_DS != INVALID)
    {
        USLOSS_Console("ERROR: Sector were NOT written successfully\n");
        USLOSS_Console("       P2_DiskSize returned with INVALID(-1)\n");
        //return INVALID;
    }
    else
    {
        // Get disk block
        disk_blocks = init_sector * init_track;
        disk_blocks *= init_disk;
        disk_blocks /= USLOSS_MmuPageSize();
        
        if(DEBUG_B && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > disk_blocks = %d\n", disk_blocks);
        }
        
        frame_cache = malloc(sizeof(Cache) * disk_blocks);
        
        // Initializing the frame cache list
        for(i = 0; i < disk_blocks; i++)
        {
            frame_cache[i].track = INVALID;
            /*
            if(DEBUG_B_DETAIL && DEBUG_GLOBAL)
            {
                USLOSS_Console("    > Initializing for disk_blocks %d\n", i);
            }
            */
            
            frame_cache[i].track = tracks;
            frame_cache[i].first = 0;
            frame_cache[i].sectors = INVALID;
            frame_cache[i].buffer = NULL;
            
            if(i % 2)
            {
                tracks++;
                frame_cache[i].first = FIRST_SECTOR;
            }
            
            frame_cache[i].status = UNUSED;
           
            /*
            if(DEBUG_B_DETAIL && DEBUG_GLOBAL)
            {
                P1_P(debug_mutex);
                USLOSS_Console("      frame_cache[%d].track = %d\n", i, frame_cache[i].track);
                USLOSS_Console("      frame_cache[%d].first = %d\n", i, frame_cache[i].first);
                USLOSS_Console("      frame_cache[%d].sectors = %d\n", i, frame_cache[i].sectors);
                USLOSS_Console("      frame_cache[%d].status = %d\n", i, frame_cache[i].status);
                P1_V(debug_mutex);
            }
             */
        }
    }
    
    P3_vmStats.blocks = disk_blocks;
    P3_vmStats.freeBlocks = disk_blocks;
    
    
    
    P3_vmStats.pages = pages;
    P3_vmStats.frames = frames;
    P3_vmStats.freeFrames = frames;
    numPages = pages;
    frame_num = frames;
    
    
    
    /* PART B */
    // Clock algorithm stuffs
    // TODO
    clock_pointer = 0;
    
    
    
    // Fixed the problem of privileged instruction
    return 0;//numPages * USLOSS_MmuPageSize();
}


/*
 *----------------------------------------------------------------------
 *
 * Pager
 *
 *	Kernel process that handles page faults and does page
 *	replacement.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */
static int Pager(void)
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ Pager: Called\n");
    }
    
    P1_V(running);  // Running semaphore
    void * data = malloc((int)USLOSS_MmuPageSize());
    
    
    
    
    int numPagesPtr;
    int vm_starting_address = (int)USLOSS_MmuRegion(&numPagesPtr);

    
    
    
NEXT:
    while(1)
    {
    
        /* Wait for fault to occur (receive from pagerMbox) */
        Fault fault;
        //void * fault_msg;
        
        int finding = 1;
        
        int fault_size = SIZE_OF_FAULT;
        P2_MboxReceive(pagerMbox, &fault, &fault_size);
        
        
        if(!pager_kill)
        {
            P1_P(mutex);    // Critical section started
            
            int aPage = getPage(fault);
            int page_address = aPage * (int)USLOSS_MmuPageSize();
            page_address += vm_starting_address;
            
            
            // Get pid
            int aPID = fault.pid;
            CheckPid(aPID);
            
            /* Find a free frame */
            if(DEBUG_A && DEBUG_GLOBAL)
            {
//                USLOSS_Console("    > Finding a free frame... ");
            }
            int i;
            int frame_index_found = INVALID;
            int a_block = INVALID;
            
            
            
            
            
            /* PART B */
            finding = 1;
            for(i = 0; i < frame_num; i++)
            {
                if((!frame_list[i].status) && finding)
                {
                    finding = 0;
                    
                    frame_list[i].status = INCORE;
                    
                    frame_index_found = i;
                    
                    P3_vmStats.freeFrames--;
                }
            }
            
            // Free frame found
            if(frame_index_found != INVALID)
            {
                // TODO
                
                P1_V(mutex);
            }
            // Do clock algorithm
            else
            {
                
                
                P1_P(clock_mutex);
                int keepGoing = 1;
                while(keepGoing)
                {
                    //extern int	USLOSS_MmuGetAccess(int frame, int *accessPtr);
                    int accessPtr;
                    int xcode_MGA = USLOSS_MmuGetAccess(clock_pointer, &accessPtr);
                    
                    if(xcode_MGA == USLOSS_MMU_OK)
                    {
                        if(USLOSS_MMU_REF & accessPtr)
                        {
                            int mmu_ref_op = ~USLOSS_MMU_REF;
                            
                            int access =  mmu_ref_op & accessPtr;
                            
                            int xcode_MGA_0 = USLOSS_MmuSetAccess(clock_pointer, access);
                            
                            if(xcode_MGA_0 == USLOSS_MMU_OK)
                            {
                                clock_pointer++;
                                clock_pointer = clock_pointer % frame_num;
                            }
                            else
                            {
                                USLOSS_Console("ERROR: Error(%d) occured with USLOSS_MmuSetAccess(%d, %d)\n", xcode_MGA_0, clock_pointer, access);
                                
                                return INVALID;
                                
                            }
                        }
                        else
                        {
                            frame_index_found = clock_pointer;
                            clock_pointer++;
                            
                            clock_pointer = clock_pointer % frame_num;
                            
                            
                            
                            keepGoing = 0;
                            break;
                        }
                    }
                    else
                    {
                        USLOSS_Console("ERROR: Error(%d) occured with USLOSS_MmuGetAccess(%d, %d)\n", xcode_MGA, clock_pointer, accessPtr);
                        
                        return INVALID;
                    }
                    
                }
                P1_V(clock_mutex);
                
                
                
                finding = 1;
                for(i = 0; i < disk_blocks; i++)
                {
                    if((!frame_cache[i].status) && finding)
                    {
                        finding = 0;
                        a_block = i;
                    }
                }
                
                // Check invalid
                if(a_block == INVALID)
                {
                    
                    USLOSS_Console("ERROR: Unable to find a free space for process %d\n", aPID);
                    
                    P1_Kill(aPID);
                    
                    USLOSS_Console("       Process %d will be killed\n", aPID);
                    
                    /* Unblock waiting (faulting) process */
                    P1_V(mutex);    // Critical section ended
                    
                    goto NEXT;
                }
                
                
                for(i = 0; i < P1_MAXPROC; i++)
                {
                    int x;
                    for(x = 0; x < numPages; x++)
                    {
                        int current_state = processes[i].pageTable[x].state;
                        int current_frame = processes[i].pageTable[x].frame;
                        
                        if(DEBUG_B && DEBUG_GLOBAL)
                        {
                            USLOSS_Console("    > frame_index_found: %d\n", frame_index_found);
                            USLOSS_Console("    > current_frame: %d\n", current_frame);
                        }
                        
                        if(current_state == INCORE && current_frame == frame_index_found)
                        {
                            processes[i].pageTable[x].state = INVALID;  // so it's in disk
                            processes[i].pageTable[x].block = a_block;
                            
                            break;
                        }
                    }
                }
                
                
                P3_vmStats.freeBlocks--;
                
                //P3_vmStats.pageIns--;
                P3_vmStats.pageOuts++;
                
                P3_vmStats.replaced++;
                
                
                int xcode_MmuMap = USLOSS_MmuMap(TAG, aPage, frame_index_found, USLOSS_MMU_PROT_RW);
               
                if(xcode_MmuMap == USLOSS_MMU_FAULT)
                {
                    USLOSS_Console("ERROR: Map MMU fault\n");
                    // Just halt plz
                    USLOSS_Halt(1);
                    //P1_V(complete);
                    //return INVALID;
                }
                // Map ok
                else if(xcode_MmuMap == USLOSS_MMU_OK)
                {
                    
                    //memset((char *)&P3_vmStats, 0, sizeof(P3_VmStats));
                    //memset((char *)page_address, 0, sizeof((int)USLOSS_MmuPageSize()));
                    //memset((char *)page_address, 0, (int)USLOSS_MmuPageSize());
                    
                    // copy to the address
                    memcpy((void *) page_address, data, (int)USLOSS_MmuPageSize());
                    
                    int xcode_MmuUmmap = USLOSS_MmuUnmap(TAG, aPage);
                    
                    // Unmap fault
                    if(xcode_MmuUmmap == USLOSS_MMU_FAULT)
                    {
                        USLOSS_Console("ERROR: Unmap MMU fault\n");
                        
                        
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    // Unmap ok
                    else if(xcode_MmuUmmap == USLOSS_MMU_OK)
                    {
                        
                        
                        
                        int track = frame_cache[a_block].track;
                        int first = frame_cache[a_block].first;
                        
                        
                        /* Unblock waiting (faulting) process */
                        P1_V(mutex);    // Critical section ended
                        
                        int xcode_DW = P2_DiskWrite(1, track, first, FIRST_SECTOR, data);
                        if(!xcode_DW)
                        {
                            USLOSS_Console("ERROR: P2_DiskWrite returned with %d\n", xcode_DW);
                            USLOSS_Halt(1);
                        }
                        
                        //USLOSS_Console(">>>>PID: %d\n", aPID);
                        //USLOSS_Console(">>>>aPage: %d\n", aPage);
                        //processes[aPID].pageTable[aPage].state = INCORE;
                        
                        
                        //USLOSS_Console(">>>>\n");
                        
                        //frame_list[i].status = INCORE;
                        
                        
                        //processes[aPID].pageTable[aPage].frame = frame_index_found;
                        
                        // DEBUG P3_vmStats
                        //printVMStats(1, "Before freeFrames--");
                        
                        //P3_vmStats.freeFrames--;
                        
                        // DEBUG P3_vmStats
                        //printVMStats(1, "After freeFrames--");
                    }
                    // Unmap unknown error
                    else
                    {
                        USLOSS_Console("ERROR: Unmap MMU unknown error\n");
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    
                    

                }
                // Map unknown error
                else
                {
                    USLOSS_Console("ERROR: Map MMU unknown error\n");
                    
                    // Just halt plz
                    USLOSS_Halt(1);
                    //P1_V(complete);
                    //return INVALID;
                }
                
                
                
                
                
                
                P1_P(frame_list[frame_index_found].mutex);  // Critical section started
                
                
                
                int status = processes[aPID].pageTable[aPage].state;
                int b_block = processes[aPID].pageTable[aPage].block;
                
                if(status)
                {
                    int track = frame_cache[b_block].track;
                    int first = frame_cache[b_block].first;
                    
                    P2_DiskRead(1, track, first, FIRST_SECTOR, data);
                    int xcode_map = USLOSS_MmuMap(TAG, aPage, frame_index_found, USLOSS_MMU_PROT_RW);
                    
                    if(xcode_map == USLOSS_MMU_OK)
                    {
                    
                    
                        // Copy to our data buffer
                        memcpy((void *)page_address, data, (int)USLOSS_MmuPageSize());
                    
                        int xcode_unmap = USLOSS_MmuUnmap(TAG, aPage);
                        if(xcode_unmap == USLOSS_MMU_OK)
                        {
                        
                            frame_cache[b_block].status = UNUSED;
                    
                            P3_vmStats.freeBlocks++;
                            P3_vmStats.pageIns++;
                        }
                        else
                        {
                            USLOSS_Console("ERROR: Unmap MMU error\n");
                            // Just halt plz
                            USLOSS_Halt(1);
                            //P1_V(complete);
                            //return INVALID;
                        }
                        
                    }
                    else
                    {
                        USLOSS_Console("ERROR: Map MMU error\n");
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    
                    
                    
                    
                    
                }
                else
                {
                    
                    int xcode_map = USLOSS_MmuMap(TAG, aPage, frame_index_found, USLOSS_MMU_PROT_RW);
                    
                    if(xcode_map == USLOSS_MMU_OK)
                    {
                        
                        
                        //memset((char *)&P3_vmStats, 0, sizeof(P3_VmStats));
                        //memset((char *)page_address, 0, sizeof((int)USLOSS_MmuPageSize()));
                        memset((char *)page_address, 0, (int)USLOSS_MmuPageSize());
                        
                        int xcode_unmap = USLOSS_MmuUnmap(TAG, aPage);
                        if(xcode_unmap == USLOSS_MMU_OK)
                        {
                            
                            P3_vmStats.new++;
                        }
                        else
                        {
                            USLOSS_Console("ERROR: Unmap MMU error\n");
                            // Just halt plz
                            USLOSS_Halt(1);
                            //P1_V(complete);
                            //return INVALID;
                        }
                        
                    }
                    else
                    {
                        USLOSS_Console("ERROR: Map MMU error\n");
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    
                }
                
                
                
                
                
                
                
            
                
                
                
                
                P1_V(frame_list[frame_index_found].mutex);  // Critical section ended
                
                
                
                
                
            }
            
            
            
            
            
            
            
            
            processes[aPID].pageTable[aPage].state = INCORE;
            
            
            
            
            
            // Page block invalid
            
            
            
            
            /*
            if(processes[aPID].pageTable[aPage].block != INVALID)
            {
                if(DEBUG_B && DEBUG_GLOBAL)
                {
                    USLOSS_Console("     > Block at %d\n", processes[aPID].pageTable[aPage].block);
                }
            }
            else
            {
                
                
                if(DEBUG_B && DEBUG_GLOBAL)
                {
                    USLOSS_Console("     > Block invalid, already in disk\n");
                }
                
                for(i = 0; i < disk_blocks; i++)
                {
                    int frame_status = frame_cache[i].status;
                    */
                    /*
                    switch(frame_status)
                    {
                        case UNUSED:
                            
                            break;
                            
                            
                        case INCORE:
                            
                            break;
                            
                        case default:
                            USLOSS_Console("ERROR: Unknown status for frame block %d", i);
                            break;
                    }
                     */
                    /*
                    if(frame_status == UNUSED)
                    {
                        finding = 0;
                        frame_index_found = i;
                        break;
                    }
                }
                
                
                if(frame_index_found == INVALID)
                {
                    USLOSS_Console("ERROR: Unable to find a free space for process %d\n", aPID);
                    
                    P1_Kill(aPID);
                    
                    USLOSS_Console("       Process %d will be killed\n", aPID);
                    
                    P1_V(mutex);
                    
                    goto NEXT;
                    
                }
                else
                {
                    frame_cache[frame_index_found].status = INCORE;
                    
                    P3_vmStats.freeBlocks--;
                    //P3_vmStats.freeFrames--;
                    
                    processes[aPID].pageTable[aPage].block = frame_index_found;
                    
                    if(DEBUG_B && DEBUG_GLOBAL)
                    {
                        USLOSS_Console("    > Process %d found index at %d\n", aPID, frame_index_found);
                    }
                }
            }
            
            
            finding = 1;
            for(i = 0; i < frame_num; i++)
            {
                
                if((!frame_list[i].status) && finding)
                {
                    finding = 0;
                    
                    // Free frame found
                    frame_index_found = i;
                    
                    frame_list[frame_index_found].status = INCORE;
                    
                    
                    P3_vmStats.freeFrames--;
                }
            }
            
            
            // Free frame found
            if(frame_index_found != INVALID)
            {
                // TODO
                
                P1_V(mutex);
            }
            // Do clock algorithm
            else
            {
                
                
                P3_vmStats.replaced++;
            }
            */
            
            
            
            
            
            
            
            
            
            
            processes[aPID].pageTable[aPage].frame = frame_index_found;
            
            
            
            
            
            /*
            for(i = 0; i < frame_num; i++)
            {
            
            
                if(!frame_list[i].status)
                {
                    // Free frame found
                    frame_index_found = i;
                
                
                    //extern int	USLOSS_MmuMap(int tag, int page, int frame, int protection);
                
                */
                
                    /*
                    #define USLOSS_MMU_PROT_NONE	0   Page cannot be accessed
                    #define USLOSS_MMU_PROT_READ	1   Page is read-only
                    #define USLOSS_MMU_PROT_RW     3   Page can be both read and written
                    */
                    /* --------------------------------------------------
                     * Map
                    * -------------------------------------------------- */
            /*
                    int xcode_MmuMap = USLOSS_MmuMap(TAG, aPage, frame_index_found, USLOSS_MMU_PROT_RW);
                
                    // Map fault
                    if(xcode_MmuMap == USLOSS_MMU_FAULT)
                    {
                        USLOSS_Console("ERROR: Map MMU fault\n");
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    // Map ok
                    else if(xcode_MmuMap == USLOSS_MMU_OK)
                    {
                    
                        int page_address = aPage * (int)USLOSS_MmuPageSize();
                        page_address += vm_starting_address;
                    
                        //memset((char *)&P3_vmStats, 0, sizeof(P3_VmStats));
                        //memset((char *)page_address, 0, sizeof((int)USLOSS_MmuPageSize()));
                        memset((char *)page_address, 0, (int)USLOSS_MmuPageSize());
                    }
                    // Map unknown error
                    else
                    {
                        USLOSS_Console("ERROR: Map MMU unknown error\n");
                    
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                
                
                
                */
                    /* --------------------------------------------------
                    * Unmap
                    * -------------------------------------------------- */
            /*
                    int xcode_MmuUmmap = USLOSS_MmuUnmap(TAG, aPage);
                
                    // Unmap fault
                    if(xcode_MmuUmmap == USLOSS_MMU_FAULT)
                    {
                        USLOSS_Console("ERROR: Unmap MMU fault\n");
                    
                    
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                    // Unmap ok
                    else if(xcode_MmuUmmap == USLOSS_MMU_OK)
                    {
                    
                        //USLOSS_Console(">>>>PID: %d\n", aPID);
                        //USLOSS_Console(">>>>aPage: %d\n", aPage);
                        processes[aPID].pageTable[aPage].state = INCORE;
                    
                    
                        //USLOSS_Console(">>>>\n");
                    
                        frame_list[i].status = INCORE;
                    
                    
                        processes[aPID].pageTable[aPage].frame = frame_index_found;
                        
                        // DEBUG P3_vmStats
                        printVMStats(1, "Before freeFrames--");
                        
                        P3_vmStats.freeFrames--;
                        
                        // DEBUG P3_vmStats
                        printVMStats(1, "After freeFrames--");
                    }
                    // Unmap unknown error
                    else
                    {
                        USLOSS_Console("ERROR: Unmap MMU unknown error\n");
                        // Just halt plz
                        USLOSS_Halt(1);
                        //P1_V(complete);
                        //return INVALID;
                    }
                
                
                
                
                
                
                    if(DEBUG_A && DEBUG_GLOBAL)
                    {
                        USLOSS_Console("OK\n");
                        USLOSS_Console("    > Free frame found at frame_list[%d]\n", i);
                    }
                    break;
                }
            }
            */
            
            
            /* If there isn't one run clock algorithm, write page to disk if necessary */
            
            
            /* Load page into frame from disk or fill with zeros */
            
         
            
            
            
            
            
     
   
        }
        else
        {
            break;
        }
        
        
        
        
        
        
        
        
        
        int size = 0;
        //void * msg = NULL;
        //P2_MboxSend(fault.mbox, msg, &size);   // Test
        P2_MboxSend(fault.mbox, NULL, &size);   // Test
    }
    
    /* Never gets here. */
    //USLOSS_Console("ERROR: Pager returned\n");
    
    P1_V(complete);
    
    return 1;
}

static int getPage(Fault fault)
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ getPage: Called\n");
        USLOSS_Console("    - Fault address: %d\n", (int)fault.addr);
        USLOSS_Console("    - Getting page... ");
    }
    int address = (int)fault.addr;
    int page_size = (int)USLOSS_MmuPageSize();
    
    int page = address/page_size;
    
    if(page < 0)
    {
        if(DEBUG_A && DEBUG_GLOBAL)
        {
            USLOSS_Console("ERROR\n");
        }
        USLOSS_Console("ERROR: Page %d is invalid\n", page);
        
        return INVALID;
    }
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("OK\n");
    }
    return page;
}

/*
 *----------------------------------------------------------------------
 *
 * P3_VmDestroy --
 *
 *	Frees all of the global data structures
 *
 * Results:
 *      None
 *
 * Side effects:
 *      The MMU is turned off.
 *
 *----------------------------------------------------------------------
 */
void P3_VmDestroy(void)
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_VmDestroy: Called\n");
    }
    
    CheckMode();
    USLOSS_MmuDone();
    /*
     * Kill the pagers here.
     */
    
    if(pagerMbox == INVALID)
    {
        return;
    }
    
    Fault fault;
    
    int fault_size = SIZE_OF_FAULT;
    
    int i = 0;
    pager_kill = 1;
    
    for(i = 0; i < pager_num; i++)
    {
        if(pager_pid_array[i] != INVALID)
        {
            if(DEBUG_A && DEBUG_GLOBAL)
            {
                USLOSS_Console("    > Killing Pager %d with PID: %d... ", i, pager_pid_array[i]);
            }
            int xcode_kill = P1_Kill(pager_pid_array[i]);
            if(xcode_kill)  // != 0
            {
                if(DEBUG_A && DEBUG_GLOBAL)
                {
                    USLOSS_Console("ERROR\n");
                }
                USLOSS_Console("ERROR: Killing Pager %d returned with %d\n", xcode_kill);
            }
            else
            {
                if(DEBUG_A && DEBUG_GLOBAL)
                {
                    USLOSS_Console("OK\n");
                }
            }
            
        }
        
        
        P2_MboxCondSend(pagerMbox, &fault, &fault_size);
        P1_P(complete);
    }
    
    
    // Print vm statistics
    dumpVMStats();
    
    
    
    // Free all stuffs & inilialize the pager mail box
    
    if(frame_list)
    {
        free(frame_list);
    }
    
    P1_SemFree(running);
    P1_SemFree(complete);
    P1_SemFree(mutex);
    
    P2_MboxRelease(pagerMbox);
    pagerMbox = -1;
    
}





/*
 *----------------------------------------------------------------------
 *
 * P3_Quit --
 *
 *	Called when a process quits and tears down the page table
 *	for the process and frees any frames and disk space used
 *      by the process.
 *
 * Results:
 *	None
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */
void P3_Quit(pid)
int		pid;
{
    if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_Quit: Called\n");
    }
    CheckMode();
    
    CheckPid(pid);
    
    // Check VMemory
    if(pagerMbox != INVALID)
    {
        
        if(ASSERT)
        {
            assert(processes[pid].numPages > 0);
            assert(processes[pid].pageTable != NULL);
        }
    
        /*
         * Free any of the process's pages that are on disk and free any page frames the
         * process is using.
         */
        P1_P(mutex);    // Critical section started
        
        
        int i = 0;
        for(i = 0; i < numPages; i++)
        {
            int processStatus = processes[pid].pageTable[i].state;
            int block = processes[pid].pageTable[i].block;
            
            // In disk
            if(block != INVALID)
            {
                frame_cache[block].status = UNUSED;
                /*
                frame_cache[i].track = INVALID;
                frame_cache[i].first = INVALID;
                */
                frame_cache[i].sectors = INVALID;
                frame_cache[i].buffer = NULL;
                
                
                P3_vmStats.freeBlocks++;
            }
            
            // In core
            if(processStatus)
            {
                int xcode_MmuUmmap = USLOSS_MmuUnmap(TAG, i);
                
                if(xcode_MmuUmmap == USLOSS_MMU_OK)
                {
                    
                    int get_frame = processes[pid].pageTable[i].frame;
                    
                    frame_list[get_frame].status = UNUSED; //0;
                    
                    // DEBUG P3_vmStats
                    printVMStats(DEBUG_A_DETAIL, "Before freeFrames++");
                    
                    P3_vmStats.freeFrames++;
                    
                    // DEBUG P3_vmStats
                    printVMStats(DEBUG_A_DETAIL, "After freeFrames++");
                    
                    
                }
                else if(xcode_MmuUmmap == USLOSS_MMU_FAULT)
                {
                    USLOSS_Console("ERROR: Unmap MMU fault\n");
                    USLOSS_Halt(1);
                }
                else
                {
                    USLOSS_Console("ERROR: Unmap unknown error\n");
                    USLOSS_Halt(1);
                }
                
            
            }
        }
        
        
        
        
        
        
        P1_V(mutex);    // Critical section ended
        
        /* Clean up the page table. */
        
        free((char *) processes[pid].pageTable);
        processes[pid].numPages = 0;
        processes[pid].pageTable = NULL;
    }
    else
    {
        if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > Virtual memory is NOT avaliable\n");
        }
        return;
    }

}















/*
 *----------------------------------------------------------------------
 *
 * P3_Fork --
 *
 *	Sets up a page table for the new process.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	A page table is allocated.
 *
 *----------------------------------------------------------------------
 */
void P3_Fork(pid)
int		pid;		/* New process */
{
    if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_Fork: Called\n");
    }
    int	i;
    
    if(pagerMbox != INVALID)
    {
        
        CheckMode();
        CheckPid(pid);
        
        
        
        processes[pid].numPages = numPages;
        processes[pid].pageTable = (PTE *) malloc(sizeof(PTE) * numPages);
    
        for(i = 0; i < numPages; i++)
        {
            processes[pid].pageTable[i].frame = -1;
            processes[pid].pageTable[i].block = -1;
            processes[pid].pageTable[i].state = UNUSED;
        }
    }
    else if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
    {
        USLOSS_Console("    > Virtual memory is NOT avaliable\n");
    }
}







/*
 *----------------------------------------------------------------------
 *
 * P3_Switch
 *
 *	Called during a context switch. Unloads the mappings for the old
 *	process and loads the mappings for the new.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The contents of the MMU are changed.
 *
 *----------------------------------------------------------------------
 */
void P3_Switch(old, new)
int		old;	/* Old (current) process */
int		new;	/* New process */
{
    
    
    if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_Switch: Called\n");
    }
    
    // Check virtual memory
    if(pagerMbox == INVALID)
    {
        if(DEBUG_A_DETAIL && DEBUG_GLOBAL)
        {
            USLOSS_Console("    > Virtual memory is NOT avaliable\n");
        }
        return;
    }
    
    int		page;
    int		status;
    
    CheckMode();
    CheckPid(old);
    CheckPid(new);
    
  
    if(old == new)
    {
        USLOSS_Console("ERROR: New process is the same process as the old process\n");
        return;
    }
    
    // DEBUG P3_vmStats
    printVMStats(DEBUG_A_DETAIL, "Before switches++");
    
    P3_vmStats.switches++;
    
    // DEBUG P3_vmStats
    printVMStats(DEBUG_A_DETAIL, "After switches++");
    
    
    for(page = 0; page < processes[old].numPages; page++)
    {
        /*
         * If a page of the old process is in memory then a mapping
         * for it must be in the MMU. Remove it.
         */
        if(processes[old].pageTable[page].state == INCORE)
        {
            if(ASSERT)
            {
                assert(processes[old].pageTable[page].frame != -1);
            }
            
            status = USLOSS_MmuUnmap(TAG, page);
            
            if (status != USLOSS_MMU_OK)
            {
                // report error and abort
                USLOSS_Console("ERROR: Unmap page(%d) failed\n", page);
                USLOSS_Halt(1);
            }
        }
    }
    for(page = 0; page < processes[new].numPages; page++)
    {
        /*
         * If a page of the new process is in memory then add a mapping
         * for it to the MMU.
         */
        if(processes[new].pageTable[page].state == INCORE)
        {
            if(ASSERT)
            {
                assert(processes[new].pageTable[page].frame != -1);
            }
            status = USLOSS_MmuMap(TAG, page, processes[new].pageTable[page].frame,
                                   USLOSS_MMU_PROT_RW);
            
            if(status != USLOSS_MMU_OK)
            {
                // report error and abort
                USLOSS_Console("ERROR: Map page(%d) failed\n", page);
                USLOSS_Halt(1);
            }
        }
    }
}



/*
 *----------------------------------------------------------------------
 *
 * FaultHandler
 *
 *	Handles an MMU interrupt.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The current process is blocked until the fault is handled.
 *
 *----------------------------------------------------------------------
 */
static void FaultHandler(type, arg)
int		type;	/* USLOSS_MMU_INT */
void	*arg;	/* Address that caused the fault */
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ FaultHandler: Called\n");
    }
    
    int		cause;
    int		status;
    Fault	fault;
    int     size;
    
    if(ASSERT)
    {
        assert(type == USLOSS_MMU_INT);
    }
    
    
    cause = USLOSS_MmuGetCause();
    
    if(ASSERT)
    {
        assert(cause == USLOSS_MMU_FAULT);
    }
    
    // DEBUG P3_vmStats
    printVMStats(1, "Before new++ & fault++");
    
    P3_vmStats.new++;
    P3_vmStats.faults++;
    
    // DEBUG P3_vmStats
    printVMStats(1, "After new++ & fault++");
    
    
    
    fault.pid = P1_GetPID();
    fault.addr = arg;
    fault.mbox = P2_MboxCreate(1, 0);
    
    if(ASSERT)
    {
        assert(fault.mbox >= 0);
    }
    
    size = sizeof(fault);
    
    status = P2_MboxSend(pagerMbox, &fault, &size);
    
    if(ASSERT)
    {
        assert(status >= 0);
        assert(size == sizeof(fault));
    }
    
    
    size = 0;
    status = P2_MboxReceive(fault.mbox, NULL, &size);
    
    if(ASSERT)
    {
        assert(status >= 0);
    }
    
    
    status = P2_MboxRelease(fault.mbox);
    
    if(ASSERT)
    {
        assert(status == 0);
    }
    
    // Print process table
    if(TOP)
    {
        P1_DumpProcesses();
    }
}







/*
 * Helper routines
 */
static void CheckPid(int pid)
{
    if((pid < 0) || (pid >= P1_MAXPROC))
    {
        USLOSS_Console("Invalid pid\n");
        USLOSS_Halt(1);
    }
}

static void CheckMode(void)
{
    if((USLOSS_PsrGet() & USLOSS_PSR_CURRENT_MODE) == 0)
    {
        USLOSS_Console("Invoking protected routine from user mode\n");
        USLOSS_Halt(1);
    }
}









int P3_Startup(void *arg)
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ P3_Startup: Called\n");
    }
    int status;
    
    Sys_Spawn("P4_Startup", P4_Startup, NULL, USLOSS_MIN_STACK * 4, 3, &pid);
    
    // These case kernel protection error, call syscall instead
    //P2_Spawn("P4_Startup", P4_Startup, NULL, USLOSS_MIN_STACK * 4, 3);
    //P2_Wait(&status);
    CheckPid(pid);
    
    Sys_Wait(&pid, &status);
    
    //Sys_VmDestory();
    
    return status;
    
}





static void dumpVMStats(void)
{
    if(DEBUG_A && DEBUG_GLOBAL)
    {
        USLOSS_Console("\n*** @ dumpVMStats: Called\n");
    }
    /*
     * Print vm statistics.
     */
    P1_P(mutex);
    
    USLOSS_Console("P3_vmStats:\n");
    USLOSS_Console("pages: %d\n", P3_vmStats.pages);
    USLOSS_Console("frames: %d\n", P3_vmStats.frames);
    USLOSS_Console("blocks: %d\n", P3_vmStats.blocks);
    /* and so on... */
    USLOSS_Console("freeFrames: %d\n", P3_vmStats.freeFrames);
    USLOSS_Console("freeBlocks: %d\n", P3_vmStats.freeBlocks);
    USLOSS_Console("switches: %d\n", P3_vmStats.switches);
    USLOSS_Console("faults: %d\n", P3_vmStats.faults);
    USLOSS_Console("new: %d\n", P3_vmStats.new);
    USLOSS_Console("pageIns: %d\n", P3_vmStats.pageIns);
    USLOSS_Console("pageOuts: %d\n", P3_vmStats.pageOuts);
    USLOSS_Console("replaced: %d\n", P3_vmStats.replaced);
    
    P1_V(mutex);
}














// DEBUG USAGE ONLY
static void printVMStats(int debug, char * str)
{
    if((DEBUG_A || DEBUG_B) && debug && PRINT_CRAPS && DEBUG_GLOBAL)
    {
        
    //USLOSS_Console("\n*** @ printVMStats: Called\n");
    /*
     * Print vm statistics.
     */
    P1_P(debug_mutex);
    if(str)
    {
        USLOSS_Console("    > %s\n", str);
    }
    USLOSS_Console("    > Printing P3_vmStats...\n");
    USLOSS_Console("      P3_vmStats.pages:\t\t%d\n", P3_vmStats.pages);
    USLOSS_Console("      P3_vmStats.frames:\t%d\n", P3_vmStats.frames);
    USLOSS_Console("      P3_vmStats.blocks:\t%d\n", P3_vmStats.blocks);
    /* and so on... */
    USLOSS_Console("      P3_vmStats.freeFrames:\t%d\n", P3_vmStats.freeFrames);
    USLOSS_Console("      P3_vmStats.freeBlocks:\t%d\n", P3_vmStats.freeBlocks);
    USLOSS_Console("      P3_vmStats.switches:\t%d\n", P3_vmStats.switches);
    USLOSS_Console("      P3_vmStats.faults:\t%d\n", P3_vmStats.faults);
    USLOSS_Console("      P3_vmStats.new:\t\t%d\n", P3_vmStats.new);
    USLOSS_Console("      P3_vmStats.pageIns:\t%d\n", P3_vmStats.pageIns);
    USLOSS_Console("      P3_vmStats.pageOuts:\t%d\n", P3_vmStats.pageOuts);
    USLOSS_Console("      P3_vmStats.replaced:\t%d\n", P3_vmStats.replaced);
    P1_V(debug_mutex);
    }
}
